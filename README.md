To install all dependencies, run:

```bash
poetry install
poetry update
```

Add `.env` file as in `.env.local`

```
DB_USER=user
DB_PASSWORD=password
DB_URL=localhost:5432
DB_NAME=name
```

To start development server, run:

```bash
fastapi dev src/main.py
```