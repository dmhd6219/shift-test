"""
This module contains the Pydantic schemas for request and response models.
These schemas are used to serialize and deserialize data from requests and
responses, and also to generate OpenAPI documentation for the FastAPI application.
"""
from datetime import date
from typing import Optional

from pydantic import BaseModel


class UserBase(BaseModel):
    """
    A base model representing the common fields of a user.

    Attributes:
        email (str): The email of the user.
        username (str): The username of the user.
    """
    email: str
    username: str


class UserCreate(UserBase):
    """
    A model representing the necessary information to create a new user.

    Inherits email and username from UserBase and adds the password
    field required for user creation.

    Attributes:
        password (str): The password for the new user account.
    """
    password: str


class User(UserBase):
    """
    A model representing a fully qualified user,
    extending the base user fields with additional data.

    Attributes:
        id (int): The unique identifier of the user.
        is_active (bool): The active status of the user.
        role (str): The role of the user.
    """
    id: int
    is_active: bool
    role: str

    class Config:  # pylint: disable=too-few-public-methods
        """
        Configuration class for the User model.

        This inner class is used to provide configuration parameters to the Pydantic model.
        Setting `orm_mode` to True allows the app to read data even if it's not a dict,
        but an ORM model (e.g., SQLAlchemy).
        """
        orm_mode = True


class UserConfidential(User):
    """
    A model representing a fully qualified user,
    extending the base user fields with additional data.

    Attributes:
        salary (int): The salary of the user.
        date_before_promotion (date): The date before promotion of the user.
    """
    salary: int
    date_before_promotion: date


class UserUpdate(BaseModel):
    """
    UserUpdate is a class derived from UserConfidential that represents the structure
    for updating user details. All attributes are optional, allowing partial updates.
    """
    email: Optional[str] = None
    username: Optional[str] = None
    is_active: Optional[bool] = None
    role: Optional[str] = None
    salary: Optional[int] = None
    date_before_promotion: Optional[date] = None
    password: Optional[str] = None
