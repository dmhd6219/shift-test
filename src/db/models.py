""" This module contains the ORM models used to represent the database structure. """

from sqlalchemy import Boolean, Column, Date, Integer, String

from .database import Base


class User(Base):  # pylint: disable=too-few-public-methods
    """
    User model representing a user in the database.

    Attributes:
        id (int): The primary key.
        username (str): The user's username, must be unique.
        email (str): The user's email address, must be unique.
        hashed_password (str): The hashed password for the user.
        is_active (bool): Flag to indicate if the user's account is active.
        role (str): The role of the user, default is 'user'.

        salary (int): The user's salary.
        date_before_promotion (Date): The date before promotion.
    """
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)
    role = Column(String, default="user")

    salary = Column(Integer)
    date_before_promotion = Column(Date)
