"""Module for handling authentication in the application."""

from datetime import datetime, timedelta, timezone
from typing import Annotated

from jose import JWTError, jwt
from fastapi import Depends, HTTPException, status

from src.db import schemas
from src.db.auth_context import pwd_context, oauth2_scheme, SECRET_KEY, ALGORITHM
from src.db.crud import get_user_by_username, get_user
from src.db.database import get_db


def authenticate_user(username: str, password: str):
    """
    Authenticate a user by their username and password.

    Parameters:
    - username (str): The username of the user to authenticate.
    - password (str): The password of the user to authenticate.

    Returns:
    - schemas.User | bool: The authenticated user object, or False if authentication fails.
    """
    with next(get_db()) as db:
        user = get_user_by_username(db, username)
        if not user:
            return False
        if not pwd_context.verify(password, user.hashed_password):
            return False
        return user


def create_token(data: dict, expires_delta: timedelta | None = None):
    """
    Create a JWT token for the given data.

    Parameters:
    - data (dict): The payload to encode in the JWT token.
    - expires_delta (timedelta | None): Optional expiration delta for the token.

    Returns:
    - str: A JWT token as a string.
    """
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.now(timezone.utc) + expires_delta
    else:
        expire = datetime.now(timezone.utc) + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def get_current_user(token: Annotated[str, Depends(oauth2_scheme)]):
    """
    Retrieve the current user from the given JWT token.

    Parameters:
    - token (str): The JWT token to decode and extract user information.

    Returns:
    - schemas.User: The user object associated with the given token.

    Raises:
    - HTTPException: If the token is invalid or the user cannot be found.
    """
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=ALGORITHM)
        user_id: str = payload.get("sub")
        if user_id is None:
            raise credentials_exception
    except JWTError as e:
        raise credentials_exception from e

    with next(get_db()) as db:
        user = get_user(db, int(user_id))
        if user is None:
            raise credentials_exception

        return user


class RoleChecker:  # pylint: disable=too-few-public-methods
    """
    A class used to check if a user has one of the allowed roles.

    Attributes:
    - allowed_roles (List[str]): A list of roles that are permitted to access a resource.

    Methods:
    - __call__: Invokes the class instance as a callable to check the user's role.
    """

    def __init__(self, allowed_roles):
        """
        Initialize the RoleChecker with allowed roles.

        Parameters:
        - allowed_roles (List[str]): A list of roles that are permitted to access a resource.
        """
        self.allowed_roles = allowed_roles

    def __call__(self, user: Annotated[schemas.User, Depends(get_current_user)]):
        """
        Check if the user has one of the allowed roles.

        Parameters:
        - user (schemas.User): The user to check.

        Returns:
        - schemas.User: The user if they have an allowed role.

        Raises:
        - HTTPException: If the user's role is not allowed.
        """
        if user.role in self.allowed_roles:
            return user
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You don't have enough permissions")
