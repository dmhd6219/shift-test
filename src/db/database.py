"""Database module for setting up the SQLAlchemy engine, session, and base model.

This module configures the SQLAlchemy engine using settings from the config module,
creates a sessionmaker for database sessions, and defines the declarative base for
the ORM models.
"""

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from src.config import get_settings

settings = get_settings()

SQLALCHEMY_DATABASE_URL = (f"postgresql://{settings.db_user}:{settings.db_password}"
                           f"@{settings.db_url}/{settings.db_name}")

engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    """
    Database dependency that creates a new database session for each request.

    Yields:
        Iterator[Session]: A generator producing a new database session,
        which is closed when the request ends.
    """
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
