"""This module provides CRUD operations for the database."""

from sqlalchemy.orm import Session

from . import models, schemas
from .auth_context import hash_password


def get_user(db: Session, user_id: int):
    """Retrieve a user from the database by their user ID.

    Parameters:
    db (Session): The database session.
    user_id (int): The unique id of the user.

    Returns:
    User: The retrieved user object or None if not found.
    """
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    """
    Retrieve a user by their email address.

    Parameters:
    db (Session): The database session.
    email (str): The email of the user to retrieve.

    Returns:
    Instance of models.User or None if the user is not found.
    """
    return db.query(models.User).filter(models.User.email == email).first()


def get_user_by_username(db: Session, username: str):
    """
    Retrieve a user by their username.

    Parameters:
    db (Session): The database session.
    username (str): The username of the user to retrieve.

    Returns:
    Instance of models.User or None if the user is not found.
    """
    return db.query(models.User).filter(models.User.username == username).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    """
    Retrieve a list of users, with optional skipping and limiting for pagination.

    Parameters:
    db (Session): The database session.
    skip (int): Number of users to skip (default is 0).
    limit (int): Maximum number of users to return (default is 100).

    Returns:
    List of instances of models.User.
    """
    return db.query(models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate):
    """
    Create a new user with the provided information.

    Parameters:
    db (Session): The database session.
    user (schemas.UserCreate): The user schema with the user information.

    Returns:
    The newly created instance of models.User.
    """
    hashed_password = hash_password(user.password)
    db_user = models.User(username=user.username, email=user.email, hashed_password=hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def update_user(db: Session, user_id: int, user: schemas.UserUpdate):
    """
    Update a user's information.

    Parameters:
    db (Session): The database session.
    user_id (int): The ID of the user to update.
    user (schemas.UserUpdate): The user schema with the updated information.

    Returns:
    The updated instance of models.User.
    """
    db_user = db.query(models.User).filter(models.User.id == user_id).first()

    if user.email:
        db_user.email = user.email

    if user.username:
        db_user.username = user.username

    if user.is_active:
        db_user.is_active = user.is_active

    if user.password:
        db_user.hashed_password = hash_password(user.password)

    if user.role:
        db_user.role = user.role

    if user.salary:
        db_user.salary = user.salary

    if user.date_before_promotion:
        db_user.date_before_promotion = user.date_before_promotion

    db.commit()
    db.refresh(db_user)

    return db_user
