"""Authentication context module for handling security and password hashing."""

from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext

from src.config import get_settings

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

SECRET_KEY = get_settings().jwt_secret_key
ALGORITHM = get_settings().jwt_algorithm


def hash_password(password: str) -> str:
    """
    Hash a password using the specified scheme in pwd_context.

    Parameters:
    password (str): The plaintext password to be hashed.

    Returns:
    str: A hashed password.
    """
    return pwd_context.hash(password)
