"""
This module serves as the main entry point for the FastAPI application,
defining the REST API endpoints for user authentication, item management,
and user operations. It includes dependencies for database connectivity,
utilizes OAuth2 for security, and integrates with SQLAlchemy for ORM-based
database interactions.

Functions:
    - login_for_access_token: Authenticates a user and issues a JWT token.
    - read_items: Fetches a paginated list of items from the database.
    - read_users: Retrieves a paginated list of users.
    - read_user: Gets a single user by ID.
    - create_item_for_user: Creates a new item associated with a user.
    - get_db: Provides a database session for each request.

The module also sets up the FastAPI application instance and includes
the necessary configurations for the API.
"""

from datetime import timedelta, date
from typing import Annotated, Optional

from fastapi import Depends, FastAPI, HTTPException, Query
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from .config import get_settings
from .db import crud, models, schemas
from .db.auth import RoleChecker, create_token, authenticate_user, get_current_user
from .db.database import engine, get_db

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


@app.get("/", response_model=None)
def hello():
    """
    Prints a greeting message.
    """
    return {"msg": "Hello World"}


@app.post("/users/",
          response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    """
    Create a new user in the database.

    Args:
        user (schemas.UserCreate): The user data to create.
        db (Session, optional): The database session dependency.

    Raises:
        HTTPException: 400 error if the email is already registered.

    Returns:
        User: The created user as defined in the schemas.User model.
    """
    db_email_user = crud.get_user_by_email(db, email=user.email)
    dm_username_user = crud.get_user_by_username(db, username=user.username)

    if db_email_user or dm_username_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return crud.create_user(db=db, user=user)


@app.get("/users/",
         dependencies=[Depends(RoleChecker(allowed_roles=["admin"]))],
         response_model=list[schemas.User])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    """
    Retrieve a list of users with optional pagination.

    Args:
        skip (int, optional): The number of users to skip (for pagination).
        limit (int, optional): The maximum number of users to return (for pagination).
        db (Session, optional): The database session dependency.

    Returns:
        list[schemas.User]: A list of user objects as defined in the schemas.User model.
    """
    users = crud.get_users(db, skip=skip, limit=limit)
    return users


@app.get("/users/{user_id}", response_model=schemas.User)
def read_user(user_id: int, db: Session = Depends(get_db)):
    """
    Retrieve a single user by user_id.

    Args:
        user_id (int): The ID of the user to retrieve.
        db (Session, optional): The database session dependency.

    Raises:
        HTTPException: 404 error if the user is not found.

    Returns:
        schemas.User: The requested user object as defined in the schemas.User model.
    """
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    return db_user


@app.get("/users/{user_id}/confidential", response_model=schemas.UserConfidential)
def read_confidential_user(user_id: int,
                           db: Session = Depends(get_db),
                           user=Depends(get_current_user)
                           ):
    """
    Retrieve a single user by user_id.

    Args:
        user_id (int): The ID of the user to retrieve.
        db (Session, optional): The database session dependency.
        user (User, optional): The current user object.

    Raises:
        HTTPException: 404 error if the user is not found.

    Returns:
        schemas.User: The requested user object as defined in the schemas.User model.
    """
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    if user_id != db_user.id and user.role != "admin":
        raise HTTPException(status_code=403, detail="Not enough permissions")

    return db_user


@app.post("/token", response_model=str)
def login_for_access_token(form_data: Annotated[OAuth2PasswordRequestForm, Depends()]):
    """
    Authenticate a user and return an access token.

    Args:
        form_data (OAuth2PasswordRequestForm): The form data containing the username and password.

    Raises:
        HTTPException: 400 error if the username or password is incorrect.

    Returns:
        str: An access token for the authenticated user.
    """
    user = authenticate_user(form_data.username, form_data.password)

    if not user:
        raise HTTPException(status_code=400, detail="Incorrect username or password")

    access_token_expires = timedelta(minutes=get_settings().access_token_expire_minutes)

    token_data = {"sub": str(user.id), "role": user.role}
    access_token = create_token(data=token_data, expires_delta=access_token_expires)

    return access_token


@app.put(
    "/users/{user_id}",
    dependencies=[Depends(RoleChecker(allowed_roles=["admin"]))],
    response_model=schemas.UserConfidential
)
def update_user(
        user_id: int,
        email: Optional[str] = Query(None),
        username: Optional[str] = Query(None),
        is_active: Optional[bool] = Query(None),
        role: Optional[str] = Query(None),
        salary: Optional[int] = Query(None),
        date_before_promotion: Optional[date] = Query(None),
        password: Optional[str] = Query(None),
        db: Session = Depends(get_db)
):  # pylint: disable=too-many-arguments
    """
    Update the details of a user.

    Args:
        user_id (int): The ID of the user to update.
        email (str, optional): The email of the user.
        username (str, optional): The username of the user.
        is_active (bool, optional): Flag to indicate if the user's account is active.
        role (str, optional): The role of the user.
        salary (int, optional): The salary of the user.
        date_before_promotion (date, optional): The date before the user's promotion.
        password (str, optional): The password of the user.
        db (Session, optional): The database session dependency.

    Returns:
        None
    """
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    user_update = schemas.UserUpdate(
        email=email,
        username=username,
        is_active=is_active,
        role=role,
        salary=salary,
        date_before_promotion=date_before_promotion,
        password=password
    )

    return crud.update_user(db, user_id=user_id, user=user_update)
