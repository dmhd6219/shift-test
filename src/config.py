"""
This module contains the application's configuration settings.

It uses Pydantic's BaseSettings for settings validation and management.
The `Settings` class inherits from `BaseSettings` and defines various
configuration parameters which can be set through environment variables
or a .env file. The settings include database credentials, JWT configuration,
and other application-specific parameters.

The `lru_cache` decorator is used to cache the results of the `get_settings`
function to avoid unnecessary recomputation.
"""

from functools import lru_cache
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):  # pylint: disable=too-few-public-methods
    """
    Application configuration class.

    This class provides a structured way to manage
    application settings using environment variables
    or a .env file for configuration.
    Pydantic's `BaseSettings` class automatically reads the environment
    variables and populates the attributes.

    Attributes:
        db_user (str): Database username.
        db_password (str): Password for the database.
        secret_key (str): Secret key for cryptographic operations.
        algorithm (str): Algorithm used for JWT encoding.
        access_token_expire_minutes (int): Expiration time for access tokens.
    """
    db_user: str
    db_password: str
    db_url: str
    db_name: str

    jwt_secret_key: str
    jwt_algorithm: str

    access_token_expire_minutes: int

    model_config = SettingsConfigDict(env_file=".env")


@lru_cache
def get_settings():
    """
    A decorator that wraps a function with a memoizing callable
    that saves up to the maxsize most recent calls.
    """
    return Settings()
