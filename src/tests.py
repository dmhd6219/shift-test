"""This module contains tests."""

from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)


def test_read_main():
    """
    A function to test the main route by sending a GET request to "/"
    and asserting the response status code is 200
    and the returned JSON message is {"msg": "Hello World"}.
    """
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World"}
